package com.example.revisichall4.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.revisichall4.R
import com.example.revisichall4.databinding.FragmentConfirmationBinding
import com.example.revisichall4.Repo.CartRepo
import com.example.revisichall4.ViewModel.CartViewModel
import com.example.revisichall4.ViewModel.ViewModelFactory
import com.example.revisichall4.database.CartDatabase

class ConfirmationFragment : Fragment() {

    private lateinit var viewModel: CartViewModel
    private lateinit var navController: NavController
    private lateinit var binding: FragmentConfirmationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentConfirmationBinding.inflate(inflater, container, false)
        val view = binding.root

        // Initialize CartViewModel using ViewModelFactory
        val application = requireActivity().application
        val cartItemDao = CartDatabase.getDatabase(requireContext()).cartItemDao()
        val cartRepo = CartRepo(cartItemDao)
        val viewModel: CartViewModel = ViewModelProvider(this, ViewModelFactory(requireActivity().application, cartRepo)).get(CartViewModel::class.java)

        // Observe LiveData for cart items
        viewModel.allCartItems.observe(viewLifecycleOwner, Observer { cartItems ->
            // Update UI with the new data (e.g., update RecyclerView)
            // You can access the list of cart items from the 'cartItems' variable
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        // Set an OnClickListener for the Lanjutkan Pembayaran button
        binding.lanjutkanButton.setOnClickListener {
            navController.navigate(R.id.action_confirmationFragment_to_orderFragment)
        }
    }
}
