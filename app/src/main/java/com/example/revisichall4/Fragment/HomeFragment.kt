package com.example.revisichall4.Fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.revisichall4.databinding.FragmentHomeBinding
import com.example.revisichall4.Adapter.FoodAdapter
import com.example.revisichall4.DetailsActivity
import com.example.revisichall4.Items.FoodItem
import com.example.revisichall4.R

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var foodAdapter: FoodAdapter
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize NavController
        navController = Navigation.findNavController(view)

        // Find the cartImageView and set an onClickListener
        binding.cartButton.setOnClickListener {
            // Navigate to ConfirmationFragment
            val action = HomeFragmentDirections.actionHomeFragmentToConfirmationFragment()
            navController.navigate(action)
        }

        // Find button2 and set an onClickListener (same as cartButton)
        binding.button2.setOnClickListener {
            // Navigate to ConfirmationFragment
            val action = HomeFragmentDirections.actionHomeFragmentToConfirmationFragment()
            navController.navigate(action)
        }

        // Set click listeners for each food item
        binding.sateAyamImg.setOnClickListener {
            navigateToDetailsActivity("Sate Ayam", 40_000)
        }

        binding.sateKambingImg.setOnClickListener {
            navigateToDetailsActivity("Sate Kambing", 50_000)
        }

        binding.dimsumImage.setOnClickListener {
            navigateToDetailsActivity("Dimsum", 20_000)
        }

        binding.ayamPanggangImage.setOnClickListener {
            navigateToDetailsActivity("Ayam Panggang", 30_000)
        }

        // In your fragment
        binding.button3.setOnClickListener {
            // Navigate to the FragmentProfile using the action defined in the navigation graph
            navController.navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun navigateToDetailsActivity(foodName: String, price: Int) {
        val intent = Intent(requireContext(), DetailsActivity::class.java)
            .putExtra("foodName", foodName)
            .putExtra("price", price)
        startActivity(intent)
    }
}
