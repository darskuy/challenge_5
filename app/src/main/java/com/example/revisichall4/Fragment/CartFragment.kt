package com.example.revisichall4.Fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.revisichall4.Adapter.FoodAdapter
import com.example.revisichall4.R
import com.example.revisichall4.databinding.FragmentCartBinding

class CartFragment : Fragment() {
    private lateinit var binding: FragmentCartBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // Example of opening Google Maps
        binding.txtGoogleMaps.setOnClickListener {
            val googleMapsUrl = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"
            openGoogleMaps(googleMapsUrl)
        }
    }


    private fun openGoogleMaps(googleMapsUrl: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(googleMapsUrl))
        intent.setPackage("com.google.android.apps.maps")


        binding.txtGoogleMaps.setOnClickListener {
            val locationUri =
                Uri.parse("geo:0,0?q=Jl. BSD Green Office Park Jl. BSD Grand Boulevard, Sampora, BSD, Kabupaten Tangerang, Banten 15345")
            val mapIntent = Intent(Intent.ACTION_VIEW, locationUri)
            mapIntent.setPackage("com.google.android.apps.maps")

            try {
                startActivity(mapIntent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}
