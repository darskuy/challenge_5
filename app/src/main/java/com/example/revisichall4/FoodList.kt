package com.example.revisichall4

import com.example.revisichall4.Items.FoodItem

data class FoodList(val foodItems: List<FoodItem>)