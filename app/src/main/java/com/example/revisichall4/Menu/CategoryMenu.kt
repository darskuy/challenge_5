package com.example.revisichall4.Menu

import com.google.gson.annotations.SerializedName

data class CategoryMenu(
    @SerializedName("data")
    val data: List<CategoryDataItem>,

    @SerializedName("message")
    val message: String?,

    @SerializedName("status")
    val status: Boolean?
)

data class CategoryDataItem(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("imageUrl")
    val imageUrl: String?,
    @SerializedName("nama")
    val name: String?,
)