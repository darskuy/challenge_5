package com.example.revisichall4.Menu

import com.google.gson.annotations.SerializedName

data class ListMenu(
    @SerializedName("data")
    val data: List<MenuListData>,
    @SerializedName("message")
    val message: String?,
    @SerializedName("status")
    val status: Boolean?
)