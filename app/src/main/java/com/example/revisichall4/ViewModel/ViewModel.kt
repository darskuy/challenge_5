package com.example.revisichall4.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.example.revisichall4.Items.CartItem
import com.example.revisichall4.Repo.CartRepo

class ViewModel(private val repository: CartRepo) : ViewModel() {

    val allCartItems: LiveData<List<CartItem>> = repository.allCartItems

    fun insertCartItem(cartItem: CartItem) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.insertCartItem(cartItem)
            }
        }
    }

    fun updateCartItem(cartItem: CartItem) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateCartItem(cartItem)
        }
    }

    fun deleteCartItem(cartItem: CartItem) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.deleteCartItem(cartItem)
            }
        }
    }

    fun deleteAllCartItems() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.deleteAllCartItems()
            }
        }
    }

    // New function to confirm an item
    fun confirmCartItem(cartItem: CartItem) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val confirmedItem = cartItem.copy(isConfirmed = true)
                repository.updateCartItem(confirmedItem)
            }
        }
    }
}