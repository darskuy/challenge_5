package com.example.revisichall4.ViewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.revisichall4.Repo.CartRepo

class ViewModelFactory(private val application: Application, private val cartRepo: CartRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CartViewModel::class.java)) {
            return CartViewModel(application, cartRepo) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}