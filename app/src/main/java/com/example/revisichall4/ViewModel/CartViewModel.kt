package com.example.revisichall4.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.revisichall4.Items.CartItem
import com.example.revisichall4.Repo.CartRepo
import com.example.revisichall4.database.CartDatabase


class CartViewModel(application: Application, private val cartRepo: CartRepo) : AndroidViewModel(application) {

    val allCartItems: LiveData<List<CartItem>> = cartRepo.allCartItems

    // Implement methods for adding, updating, and deleting cart items
    fun insertCartItem(cartItem: CartItem) {
        cartRepo.insertCartItem(cartItem)
    }

    fun updateCartItem(cartItem: CartItem) {
        cartRepo.updateCartItem(cartItem)
    }

    fun deleteCartItem(cartItem: CartItem) {
        cartRepo.deleteCartItem(cartItem)
    }

    fun deleteAllCartItems() {
        cartRepo.deleteAllCartItems()
    }
    // Function to provide a ViewModel
    fun provideViewModel(fragment: Fragment): CartViewModel {
        val application = fragment.requireActivity().application
        val cartItemDao = CartDatabase.getDatabase(fragment.requireContext()).cartItemDao()
        val cartRepo = CartRepo(cartItemDao)
        return ViewModelProvider(fragment, ViewModelFactory(application, cartRepo)).get(CartViewModel::class.java)
    }
}
