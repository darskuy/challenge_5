package com.example.revisichall4.database

import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import androidx.room.Database
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.revisichall4.Items.CartItem


@Database(entities = [CartItem::class], version = 2, exportSchema = false)
abstract class CartDatabase : RoomDatabase() {
    abstract fun cartItemDao(): CartItemDao

    companion object {
        @Volatile
        private var INSTANCE: CartDatabase? = null

        fun getDatabase(context: Context): CartDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CartDatabase::class.java,
                    "cart_database"
                ).addMigrations(MIGRATION_1_2)
                    .build()

                INSTANCE = instance
                return instance
            }
        }

        // Define the migration from version 1 to version 2
        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Modify the migration logic to create the table with the correct columns
                database.execSQL("CREATE TABLE IF NOT EXISTS `cart_item` " +
                        "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                        "`foodName` TEXT NOT NULL, " +
                        "`totalPrice` INTEGER NOT NULL, " +
                        "`price` INTEGER NOT NULL, " +
                        "`quantity` INTEGER NOT NULL, " +
                        "`imageResourceId` INTEGER NOT NULL)")
            }
        }
    }
}