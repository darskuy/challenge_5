package com.example.revisichall4

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.revisichall4.Items.CartItem
import com.example.revisichall4.ViewModel.CartViewModel
import com.example.revisichall4.ViewModel.ViewModelFactory
import com.example.revisichall4.database.CartDatabase
import com.example.revisichall4.databinding.ActivityDetailsBinding
import com.example.revisichall4.Repo.CartRepo

class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding
    private lateinit var googleMapsUrl: String
    private var quantity = 0
    private var priceItem = 0
    private var totalPrice = 0
    private lateinit var viewModel: CartViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val cartItemDao = CartDatabase.getDatabase(applicationContext).cartItemDao()
        val cartRepo = CartRepo(cartItemDao)
        viewModel = ViewModelProvider(this, ViewModelFactory(application, cartRepo)).get(CartViewModel::class.java)

        val foodName = intent?.getStringExtra("foodName")
        val price = intent?.getIntExtra("price", 0)
        val description = intent.getStringExtra("description")
        val imageRes = intent.getIntExtra("imageRes", 0)
        val restaurantAddress = intent.getStringExtra("restaurantAddress")
        googleMapsUrl = intent.getStringExtra("googleMapsUrl") ?: ""

        if (binding.btnAddToCart.text == "Tambah Ke Keranjang - Rp.0") {
            binding.btnAddToCart.text = "Tambah Ke Keranjang - Rp. $price"
        }

        priceItem = price ?: 0

        updateUI(foodName, price.toString(), description, imageRes, restaurantAddress)

        // Tambahkan onClickListener untuk tombol kembali
        binding.btback.setOnClickListener {
            finish()
        }

        // Tambahkan onClickListener untuk tombol tambah
        binding.btnPlus.setOnClickListener {
            quantity++
            updateQuantity(quantity)
        }

        // Tambahkan onClickListener untuk tombol kurang
        binding.btnMinus.setOnClickListener {
            if (quantity > 1) {
                quantity--
                updateQuantity(quantity)
            }
        }

        //Tambahkan onClickListener untuk tombol "Tambah Ke Keranjang"
        binding.btnAddToCart.setOnClickListener {
            val cartItem = CartItem(
                foodName = foodName ?: "",
                totalPrice = totalPrice,
                price = price ?: 0,
                quantity = quantity,
                imageResourceId = imageRes!!
            )
            if (quantity > 0) {
                viewModel.insertCartItem(cartItem)
                Toast.makeText(this@DetailsActivity, "Item added to cart", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@DetailsActivity, "Quantity cannot be 0!", Toast.LENGTH_SHORT).show()
            }
        }

        binding.txtGoogleMaps.setOnClickListener {
            // Memeriksa apakah URL Google Maps tidak kosong
            if (googleMapsUrl.isNotEmpty()) {
                // Membuka tautan Google Maps di browser
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(googleMapsUrl))
                startActivity(intent)
            }
        }
    }

    private fun updateQuantity(quantity: Int) {
        binding.txtQuantity.text = quantity.toString()
        totalPrice = quantity * priceItem
        binding.btnAddToCart.text = "Tambah Ke Keranjang - Rp. $totalPrice"
    }

    private fun updateUI(foodName: String?, price: String?, description: String?, imageRes: Int, restaurantAddress: String?) {
        binding.apply {
            imgFood.setImageResource(imageRes)
            txtFoodName.text = foodName
            txtFoodPrice.text = "Rp. ${price ?: "0"}" // Provide a default value if price is null
            txtFoodDescription.text = description
            txtLocation.text = restaurantAddress
            txtGoogleMaps.text = googleMapsUrl
        }
    }
}