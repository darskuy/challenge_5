package com.example.revisichall4.Repo

import androidx.lifecycle.LiveData
import com.example.revisichall4.database.CartItemDao
import com.example.revisichall4.Items.CartItem

class CartRepo (private val cartDao: CartItemDao) {

    val allCartItems: LiveData<List<CartItem>> = cartDao.getAllCartItems()

    fun insertCartItem(cartItem: CartItem) {
        cartDao.insertCartItem(cartItem)
    }

    fun deleteCartItem(cartItem: CartItem) {
        cartDao.deleteCartItem(cartItem)
    }

    fun updateCartItem(cartItem: CartItem) {
        cartDao.updateCartItem(cartItem)
    }

    fun deleteAllCartItems() {
        cartDao.deleteAllCartItems()
    }
}