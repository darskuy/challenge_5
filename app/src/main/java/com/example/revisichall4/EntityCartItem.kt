package com.example.revisichall4

data class EntityCartItem(val itemId: Int, val itemName: String, val itemPrice: Double, val itemQuantity: Int)