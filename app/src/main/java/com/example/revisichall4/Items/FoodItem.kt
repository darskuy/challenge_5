package com.example.revisichall4.Items

data class FoodItem(val name: String, val price: Int, val description: String, val imageRes: Int, val restaurantAddress: String, val googleMapsUrl: String = "")