package com.example.revisichall4

import android.content.Context

class SharedPreferencesManager(context: Context) {
    private val sharedPreferences =
        context.getSharedPreferences("MyAppPreferences", Context.MODE_PRIVATE)

    fun setViewMode(isGrid: Boolean) {
        sharedPreferences.edit().putBoolean("isGrid", isGrid).apply()
    }

    fun isGridMode(): Boolean {
        return sharedPreferences.getBoolean("isGrid", true)
    }
}
