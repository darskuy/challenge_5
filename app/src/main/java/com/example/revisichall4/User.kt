package com.example.revisichall4

data class User(
    val userId: String? = "",
    val username: String = "",
    val email: String = "",
    val phone: String = ""
)